# Ideas

- Factor in earnings somewhere
    - create a new type: earning []
- Breakdown of spendings
    - types: food, entertainment, administration, car
    - payment type: cash, credit, debit
    - credit card payment if present
		- no need for time-based comparisons
		- functions to take out credit card transactions and sum their amounts
		  [x]
		- functions to update those transactions [x]
			- ~~tentatively delete those lines and append those lines with
			  updated paid status []~~
                - delete those lines and re-create their transactions but with
              credit card as paid [x]
            - look into other ways to update these transactions; perhaps use of
              database is more relevant
    - number of debit transactions (left?) since there is a 25 limit []
		- only need current month and year
- Analysis
    - Intervals: monthly, annually
    - Amount per type across the intervals, perhaps proportions as well?
	- Amount per payment type across intervals
    - Current months' spendings (separated by type) compared to average and
      within standard deviation
    - Top transactions per type, 10 for now?
		- for a given month


'''
imports:
    - from columns A to F

first_set:
    - Past, January, February, March
    - import from row 1 onwards, assign purpose

second_set:
    - April to November
    - import from row 2 onwards

third_set:
    - December to January17
    - import from row 3 onwards

fourth_set:
    - February17 to July18
    - import from row 3 onwards, append income from j2 onwards until hit empty
      cell or 'Current Amount'

fifth_set
    - Aug18 to Sept18
    - import from row 3 onwards, append income from j33 onwards until empty
      cell

sixth_set:
    - Oct18 to present
    - import from row 3 onwards, append income from j33 onwards until empty
      cell
'''

def main(workbook):
    print(len(workbook.sheetnames))
    for sheet in workbook.sheetnames:
        if sheet in ['January', 'February', 'March']:
            pass
        elif sheet in ['April', 'May', 'June', 'July', 'August', 'September',
                'October', 'November']:
            pass
        elif sheet in ['December', 'January17']:
            pass
        elif sheet in ['Aug18', 'Sept18']:
            pass
        elif sheet in ['Oct18', 'Nov18', 'Dec18', 'Jan19', 'Feb19', 'Mar19']:
            pass
        else:
            pass

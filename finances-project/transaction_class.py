import datetime

class Transaction:

    def __init__(self, id, trans):
        self.id = id
        self.date = datetime.date.fromisoformat(trans[0])
        self.transaction = trans[1]
        self.amount = float(trans[2])
        self.method = trans[3]
        self.credit_paid = trans[4]
        self.purpose = trans[5]
        self.comment = trans[6]

    def make_credit_paid_to_y(self):
        self.credit_paid = 'Y'

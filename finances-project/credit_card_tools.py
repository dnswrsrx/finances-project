import datetime

# Credit card payment stuff
def sum_unpaid_credit_card_trasactions(transactions):
    return sum(transaction.amount for transaction in
            unpaid_credit_card_transactions(transactions))

def unpaid_credit_card_transactions(transactions):
    return [transaction for transaction in transactions if
            unpaid_credit_card_condition(transaction)]

def unpaid_credit_card_condition(transaction):
    return transaction.method == 'Credit' and transaction.credit_paid == 'N'

# Change entry for credit card (when credit card paid off)
def id_of_unpaid_credit_card_transactions(transactions):
    return [transaction.id for transaction in
            unpaid_credit_card_transactions(transactions)]

def change_credit_card_transactions_to_paid(transactions):
    for transaction in transactions:
        if transaction.id in\
        id_of_unpaid_credit_card_transactions(transactions):
            transaction.make_credit_paid_to_y()
    return transactions
